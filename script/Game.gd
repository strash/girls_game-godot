extends Control


export (PackedScene) var IcPart


signal menu_button_pressed


enum { WIN, IDLE }


onready var Board: Control = get_node("Board")
onready var Title: Label = get_node("Title")
onready var Moves: Label = get_node("Moves")
onready var MyTween: Tween = get_node("Tween")
onready var MyParticle: CPUParticles2D = get_node("CPUParticles2D")


var level: int # 1, 2, 3
var moves: int = 0 setget set_moves, get_moves # количество ходов
var active_icon: TextureRect = null
var neighbours: Array = []
var level_state: int = IDLE

# подгруженные текстуры
var textures: = []
# свойства уровней
const PROPS: = [
	{
		count_x = 3,
		count_y = 4,
		w = 134,
		h = 125,
		total = 12,
		level_title = "Easy",
	},
	{
		count_x = 4,
		count_y = 5,
		w = 100,
		h = 100,
		total = 20,
		level_title = "Medium",
	},
	{
		count_x = 5,
		count_y = 6,
		w = 80,
		h = 84,
		total = 30,
		level_title = "Hard",
	},
]


# BUILTINS - - - - - - - - -


func _ready() -> void:
	# подгрузка текстур иконок
	for i in PROPS.size():
		textures.append([])
		for j in PROPS[i].total:
			textures[i].append(load("res://assets/image/ic_%s/ic_%s_%s.png" % [i + 1, i + 1, j + 1]))


# клик по картинкам
func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.pressed and level_state == IDLE:
		var rect = Board.get_rect()
		var cur = get_local_mouse_position() - rect.position
		if cur.x >= 0 and cur.y >= 0 and cur.x <= rect.size.x and cur.y <= rect.size.y:
			find_icon(cur)


# METHODS - - - - - - - - -


func set_moves(value: int) -> void:
	moves = value
	Moves.text = "Moves: %s" % moves

func get_moves() -> int:
	return moves


# подготовка уровня
func prepare_level(new_level: int) -> void:
	clear_level()
	level = new_level
	load_board()
	Title.text = "Level %s. %s" % [level, PROPS[level - 1].level_title]


# подгрузка картинок
func load_board() -> void:
	var icons = []
	for i in textures[level - 1].size():
		var icon = IcPart.instance()
		icon.texture = textures[level - 1][i]
		icon.index = i
		icons.append(icon)
	icons.shuffle()
	for i in icons.size():
		icons[i].x = i % PROPS[level - 1].count_x
		icons[i].y = floor(i / PROPS[level - 1].count_x)
		var x = PROPS[level - 1].w * icons[i].x
		var y = PROPS[level - 1].h * icons[i].y
		icons[i].set_position(Vector2(x, y))
		Board.add_child(icons[i])
	icons.clear()
	var w = PROPS[level - 1].w * PROPS[level - 1].count_x
	var h = PROPS[level - 1].h * PROPS[level - 1].count_y
	Board.set_size(Vector2(w, h))
	Board.set_position((self.get_rect().size - Board.get_rect().size) / 2)


# очистка уровня
func clear_level() -> void:
	for i in get_tree().get_nodes_in_group("icon_group"):
		i.queue_free()
	Board.set_size(Vector2(0.0, 0.0))
	set_moves(0)
	level_state = IDLE
	MyParticle.emitting = false
	MyParticle.position.x = self.get_rect().size.x / 2


# поиск картинки и выделение
func find_icon(cur: Vector2) -> void:
	var x = floor(cur.x / PROPS[level - 1].w)
	var y = floor(cur.y / PROPS[level - 1].h)
	for i in get_tree().get_nodes_in_group("icon_group"):
		# активная иконка
		if i.x == x and i.y == y:
			if active_icon == i:
				_unselect_icon()
				return
			elif _is_neighbour(i):
				_move_icons(i)
				_check_pattern()
				_unselect_icon()
				return
			else:
				_unselect_icon()
				active_icon = i
				active_icon.set_as_selected(true)
	if active_icon:
		find_neigbors(x, y)


# нахождение соседей
func find_neigbors(x: int, y: int) -> void:
	for i in get_tree().get_nodes_in_group("icon_group"):
		# левый и правый соседи
		if i.x == x - 1 and i.y == y or i.x == x + 1 and i.y == y:
			i.set_as_neighbour(true)
			neighbours.append(i)
		# верхний и нижний соседи
		if i.x == x and i.y == y - 1 or i.x == x and i.y == y + 1:
			i.set_as_neighbour(true)
			neighbours.append(i)


# снятие выделения иконки
func _unselect_icon() -> void:
	if active_icon:
		active_icon.set_as_selected(false)
		active_icon = null
		for i in neighbours:
			i.set_as_neighbour(false)
		neighbours.clear()


# проверка на клик по соседу
func _is_neighbour(icon: TextureRect) -> bool:
	for i in neighbours:
		if i == icon:
			return true
	return false


# перемещение иконок
func _move_icons(neighbour: TextureRect) -> void:
	var n_pos = neighbour.get_rect().position
	var a_pos = active_icon.get_rect().position
	var n_indexes = { x = neighbour.x, y  = neighbour.y }
	var a_indexes = { x = active_icon.x, y  = active_icon.y }

	active_icon.x = n_indexes.x
	active_icon.y = n_indexes.y
	neighbour.x = a_indexes.x
	neighbour.y = a_indexes.y

	var _ia = MyTween.interpolate_property(active_icon, "rect_position", a_pos, n_pos, 0.2)
	var _in = MyTween.interpolate_property(neighbour, "rect_position", n_pos, a_pos, 0.2)
	if not MyTween.is_active():
		var _s = MyTween.start()

	set_moves(get_moves() + 1)


func _check_pattern() -> void:
	var check: bool = true
	var icons = get_tree().get_nodes_in_group("icon_group")
	for i in icons.size():
		var current_i = icons[i].x + icons[i].y * PROPS[level - 1].count_x
		if icons[i].index != current_i:
			check = false
	if check:
		level_state = WIN
		$CPUParticles2D.emitting = true
	else:
		level_state = IDLE


# SIGNALS - - - - - - - - -


func _on_BtnMenu_pressed() -> void:
	clear_level()
	emit_signal("menu_button_pressed")
