extends TextureRect


onready var Overlay: ColorRect = get_node("ColorRect")
onready var Player: AnimationPlayer = get_node("AnimationPlayer")


# индекс пазла в картинке
var index: int
# индексы позиции на доске
var x: int
var y: int


# BUILTINS - - - - - - - - -


func _ready() -> void:
	Overlay.hide()
	Player.stop()


# METHODS - - - - - - - - -


func set_as_selected(selected: bool) -> void:
	if selected:
		Overlay.show()
		Player.play("selected")
	else:
		Overlay.hide()
		Player.stop()


func set_as_neighbour(selected: bool) -> void:
	if selected:
		Overlay.show()
		Player.play("near")
	else:
		Overlay.hide()
		Player.stop()
