extends Control


signal newgame_pressed
signal levels_pressed


onready var MyParticle = get_node("Logo/CPUParticles2D")


func _ready() -> void:
	MyParticle.emitting = true


func _on_BtnStart_pressed() -> void:
	emit_signal("newgame_pressed")


func _on_BtnLevel_pressed() -> void:
	emit_signal("levels_pressed")
