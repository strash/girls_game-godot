extends Control


signal level1_pressed
signal level2_pressed
signal level3_pressed


func _on_BtnEasy_pressed() -> void:
	emit_signal("level1_pressed")


func _on_BtnMedium_pressed() -> void:
	emit_signal("level2_pressed")


func _on_BtnHard_pressed() -> void:
	emit_signal("level3_pressed")
