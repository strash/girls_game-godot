extends Node


onready var MyStart: Control = get_node("Start")
onready var MyLevels: Control = get_node("Levels")
onready var MyGame: Control = get_node("Game")


func _ready() -> void:
	var _seed = rand_seed(1)

	var _newgame_pressed = MyStart.connect("newgame_pressed", self, "_on_MyStart_newgame_pressed")
	var _levels_pressed = MyStart.connect("levels_pressed", self, "_on_MyStart_levels_pressed")

	var _level1_pressed = MyLevels.connect("level1_pressed", self, "_on_MyLevels_level1_pressed")
	var _level2_pressed = MyLevels.connect("level2_pressed", self, "_on_MyLevels_level2_pressed")
	var _level3_pressed = MyLevels.connect("level3_pressed", self, "_on_MyLevels_level3_pressed")

	var _menu_button_pressed = MyGame.connect("menu_button_pressed", self, "_on_MyGame_menu_button_pressed")

	set_view(1)


func set_view(view: int) -> void:
	match view:
		1:
			MyStart.show()
			$Start/Logo/CPUParticles2D.emitting = true
			MyLevels.hide()
			MyGame.hide()
		2:
			MyStart.hide()
			$Start/Logo/CPUParticles2D.emitting = false
			MyLevels.show()
			MyGame.hide()
		3:
			MyStart.hide()
			$Start/Logo/CPUParticles2D.emitting = false
			MyLevels.hide()
			MyGame.show()


func _on_MyStart_newgame_pressed() -> void:
	MyGame.prepare_level(1)
	set_view(3)


func _on_MyStart_levels_pressed() -> void:
	set_view(2)


func _on_MyLevels_level1_pressed() -> void:
	MyGame.prepare_level(1)
	set_view(3)


func _on_MyLevels_level2_pressed() -> void:
	MyGame.prepare_level(2)
	set_view(3)


func _on_MyLevels_level3_pressed() -> void:
	MyGame.prepare_level(3)
	set_view(3)


func _on_MyGame_menu_button_pressed() -> void:
	set_view(1)
